/**
 ******************************************************************************
 * @file    Binary_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/NMEA/NMEA_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace Connectivity
{
   static I_NMEA* p_NMEA_Impl = NULL;

   const char* NMEA_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle NMEA functions";
   }

   void stub_setImpl(I_NMEA* pNewImpl)
   {
      p_NMEA_Impl = pNewImpl;
   }

   static I_NMEA* NMEA_Stub_Get_Impl(void)
   {
      if(p_NMEA_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to NMEA functions. Did you forget"
                   << "to call Stubs::Connectivity::stub_setImpl() ?" << std::endl;

         throw NMEA_StubImplNotSetException();
      }

      return p_NMEA_Impl;
   }

      ///////////////////////////////
      // I_NMEA Methods Definition //
      ///////////////////////////////

   I_NMEA::~I_NMEA()
   {
      if(p_NMEA_Impl == this)
      {
         p_NMEA_Impl = NULL;
      }
   }

   ///////////////////////////////////
   // _Mock_NMEA Methods Definition //
   ///////////////////////////////////

   _Mock_NMEA::_Mock_NMEA()
   {
   }

               //////////////////////////////////
               // Definition of Non Stub Class //
               //////////////////////////////////

   NMEA::NMEA()
   {
   }

   NMEA::~NMEA()
   {
   }

   const NeoGPS::gps_fix NMEA::read(void)
   {
      return NMEA_Stub_Get_Impl()->read();
   }

   NMEA::decode_t NMEA::handle(uint8_t c)
   {
      return NMEA_Stub_Get_Impl()->handle(c);
   }

   NMEA::decode_t NMEA::decode(char c)
   {
      return NMEA_Stub_Get_Impl()->decode(c);
   }
}

