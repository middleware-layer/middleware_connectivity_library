/**
 ******************************************************************************
 * @file    NMEA_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_NMEA_NMEA_STUB_HPP
#define SOURCE_TESTING_STUBS_NMEA_NMEA_STUB_HPP

#include <Production/NMEA/NMEA.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Connectivity
{
   class NMEA;

   class NMEA_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_NMEA
   {
      public:
         virtual ~I_NMEA();

         virtual const NeoGPS::gps_fix read(void) = 0;
         virtual NMEA::decode_t handle(uint8_t c) = 0;
         virtual NMEA::decode_t decode(char c) = 0;
   };

   class _Mock_NMEA : public I_NMEA
   {
      public:
         _Mock_NMEA();

         MOCK_CONST_METHOD0(read,   NeoGPS::gps_fix   ());

         MOCK_METHOD1(handle, NMEA::decode_t (uint8_t));
         MOCK_METHOD1(decode, NMEA::decode_t (char));
   };

   typedef ::testing::NiceMock<_Mock_NMEA> Mock_NMEA;

   void stub_setImpl(I_NMEA* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_NMEA_NMEA_STUB_HPP
