/**
 ******************************************************************************
 * @file    NMEA_test.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2018
 * @brief
 ******************************************************************************
 */

#include <gtest/gtest.h>

#include <Production/NMEA/NMEA.hpp>

#include <string>

namespace Connectivity
{
   class NMEATest : public ::testing::Test
   {
      private:

      protected:
         NMEA m_nmea;

      public:
         /**
          * @brief	This method will run always before each test
          */
         void SetUp(void)
         {

         }

         /**
          * @brief	This method will run always after each test
          */
         void TearDown(void)
         {

         }

         virtual void decodeMessage(const std::string& message)
         {
            for(uint16_t i = 0; i < message.size(); i++)
            {
               this->m_nmea.decode(message.at(i));
            }
         }
   };

   TEST_F(NMEATest, GPGGA_Decode_CorrectSatteliteNumber)
   {
      std::string message = "$GPGGA,134658.00,5106.9792,N,11402.3003,W,2,09,1.0,1048.47,M,-16.27,M,08,AAAA*60";

      this->decodeMessage(message);

      ASSERT_EQ(9,
                this->m_nmea.fix().satellites);
   }

   TEST_F(NMEATest, GPGGA_Decode_CorrectHDOP)
   {
      std::string message = "$GPGGA,134658.00,5106.9792,N,11402.3003,W,2,09,1.0,1048.47,M,-16.27,M,08,AAAA*60";

      this->decodeMessage(message);

      ASSERT_EQ(1000,
                this->m_nmea.fix().hdop);
   }

   TEST_F(NMEATest, GPGLL_Decode)
   {
      std::string message = "$GPGLL,5107.0013414,N,11402.3279144,W,205412.00,A,A*73";

      this->decodeMessage(message);
   }

   TEST_F(NMEATest, GPGSA_Decode)
   {
      std::string message = "$GPGSA,M,3,17,02,30,04,05,10,09,06,31,12,,,1.2,0.8,0.9*35";

      this->decodeMessage(message);
   }

   TEST_F(NMEATest, GPGST_Decode)
   {
      std::string message = "$GPGST,141451.00,1.18,0.00,0.00,0.0000,0.00,0.00,0.00*6B";

      this->decodeMessage(message);
   }

   TEST_F(NMEATest, GPGSV_Decode)
   {
      std::string message = "$GPGSV,3,1,11,18,87,050,48,22,56,250,49,21,55,122,49,03,40,284,47*78";
      //std::string example_2 = "$GPGSV,3,2,11,19,25,314,42,26,24,044,42,24,16,118,43,29,15,039,42*7E";
      //std::string example_3 = "$GPGSV,3,3,11,09,15,107,44,14,11,196,41,07,03,173,*4D";

      this->decodeMessage(message);
   }

   TEST_F(NMEATest, GPRMC_Decode)
   {
      std::string message = "$GPRMC,144326.00,A,5107.0017737,N,11402.3291611,W,0.080,323.3,210307,0.0,E,A*20";

      this->decodeMessage(message);
   }

   TEST_F(NMEATest, GPVTG_Decode)
   {
      std::string message = "$GPVTG,172.516,T,155.295,M,0.049,N,0.090,K,D*2B";

      this->decodeMessage(message);
   }

   TEST_F(NMEATest, GPZDA_Decode)
   {
      std::string message = "$GPZDA,143042.00,25,08,2005,,*6E";

      this->decodeMessage(message);
   }
}
