**Description:** [Description of the issue]

**1. Steps to Reproduce**

* First Step
* Second Step
* and so on...

**2. Expected behavior:** [What you expect to happen]

**3. Actual behavior:** [What actually happens]

**4. Reproduces how often:** [What percentage of the time does it reproduce?]